﻿using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static DocuSign.eSign.Client.Auth.OAuth;

namespace DocuSign.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        protected static ApiClient _apiClient { get; private set; }
        private OAuthToken _authToken;

        public string EnvelopeId { get; set; }


        public AccountController(IConfiguration configuration)
        {
            _apiClient = _apiClient ?? new ApiClient("https://demo.docusign.net/restapi");
            _configuration = configuration;
        }

        [HttpPost]
        public void Completed(string xml)
        {
            Response.Redirect("http://docuport.eastus.cloudapp.azure.com/docusign/completed");
         }

        [HttpGet]
        public IActionResult Login(string returnUrl = "https://localhost:44339/")
        {
            //var b = _apiClient.GetAuthorizationUri("945396b2-0e7e-45fd-aa19-7335574b83da", new System.Collections.Generic.List<string>() { "signature" }, "https://localhost:44339/account/code", "code");
            //Response.Redirect(b.ToString());
            return Challenge(new AuthenticationProperties() { RedirectUri = returnUrl });
        }

        public void Code(string code)
        {
            this._authToken = _apiClient.GenerateAccessToken("945396b2-0e7e-45fd-aa19-7335574b83da", "cf5f486e-f510-4502-8a9c-1248c86e3ce2", code);

            EnvelopeDefinition envelope = MakeEnvelope("ceyhunahmadli91@gmail.com", "Ceyhun A.");

            // Call DocuSign to create the envelope                   
            var config = new Configuration("https://demo.docusign.net/restapi");
            config.AddDefaultHeader("Authorization", "Bearer " + this._authToken.access_token);

            AuthenticationApi authApi = new AuthenticationApi(_apiClient);
            LoginInformation loginInfo = authApi.Login();
            // user might be a member of multiple accounts
            var accountId = loginInfo.LoginAccounts[0].AccountId;



            EnvelopesApi envelopesApi = new EnvelopesApi(_apiClient);
            EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, envelope);
            string envelopeId = results.EnvelopeId;
            EnvelopeId = envelopeId;
            // Save for future use within the example launcher
            //RequestItemsService.EnvelopeId = envelopeId;



            // Create the recipient view, the Signing Ceremony
            RecipientViewRequest viewRequest = MakeRecipientViewRequest("ceyhunahmadli91@gmail.com", "Ceyhun A.", envelopeId, envelope.Documents[0].DocumentId);
            // Call the CreateRecipientView API
            ViewUrl results1 = envelopesApi.CreateRecipientView(accountId, envelopeId, viewRequest);
            TempData["EnvId"] = envelopeId;
            TempData["DocId"] = envelope.Documents[0].DocumentId;

            // Redirect the user to the Signing Ceremony
            // Don't use an iFrame!
            // State can be stored/recovered using the framework's session or a
            // query parameter on the returnUrl (see the makeRecipientViewRequest method)
            string redirectUrl = results1.Url;
            Response.Redirect(redirectUrl);
        }

        private EnvelopeDefinition MakeEnvelope(string signerEmail, string signerName)
        {
            // Data for this method
            // signerEmail 
            // signerName
            // signerClientId -- class global
            // Config.docPdf


            byte[] buffer = System.IO.File.ReadAllBytes(@"C:\Users\ceyhu\Desktop\Web.pdf");

            EnvelopeDefinition envelopeDefinition = new EnvelopeDefinition();
            envelopeDefinition.EmailSubject = "Please sign this document";
            Document doc1 = new Document();

            String doc1b64 = Convert.ToBase64String(buffer);

            doc1.DocumentBase64 = doc1b64;
            doc1.Name = "Imposta_document"; // can be different from actual file name
            doc1.FileExtension = "png";
            doc1.DocumentId = "3";

            // The order in the docs array determines the order in the envelope
            envelopeDefinition.Documents = new List<Document> { doc1 };

            // Create a signer recipient to sign the document, identified by name and email
            // We set the clientUserId to enable embedded signing for the recipient
            // We're setting the parameters via the object creation
            Signer signer1 = new Signer
            {
                Email = signerEmail,
                Name = signerName,
                ClientUserId = "A111",
                RecipientId = "1"
            };

            // Create signHere fields (also known as tabs) on the documents,
            // We're using anchor (autoPlace) positioning
            //
            // The DocuSign platform seaches throughout your envelope's
            // documents for matching anchor strings.
            SignHere signHere1 = new SignHere
            {
                AnchorString = "/sn1/",
                AnchorUnits = "pixels",
                AnchorXOffset = "10",
                AnchorYOffset = "20"
            };
            // Tabs are set per recipient / signer
            Tabs signer1Tabs = new Tabs
            {
                SignHereTabs = new List<SignHere> { signHere1 }
            };
            signer1.Tabs = signer1Tabs;

            // Add the recipient to the envelope object
            Recipients recipients = new Recipients
            {
                Signers = new List<Signer> { signer1 }
            };
            envelopeDefinition.Recipients = recipients;

            // Request that the envelope be sent by setting |status| to "sent".
            // To request that the envelope be created as a draft, set to "created"
            envelopeDefinition.Status = "sent";


            return envelopeDefinition;
        }

        public ActionResult DownloadSignedDocument(string envelopeId, string documentId)
        {
            EnvelopesApi envelopesApi = new EnvelopesApi(_apiClient);


            AuthenticationApi authApi = new AuthenticationApi(_apiClient);
            LoginInformation loginInfo = authApi.Login();
            // user might be a member of multiple accounts
            var accountId = loginInfo.LoginAccounts[0].AccountId;

            // var r = envelopesApi.GetEnvelope("95a680ef-4b12-42df-8c5b-aed59a745e58", "5119e39a-d827-461d-aecd-d9461f45d7b5");
            Stream results3 = envelopesApi.GetDocument(accountId, envelopeId, documentId);

            //string contentType;
            //new FileExtensionContentTypeProvider().TryGetContentType("a." + extension, out contentType);
            //var a = contentType ?? "application/octet-stream";

            return File(results3, "application/pdf", "abc." + "pdf");
        }

        private RecipientViewRequest MakeRecipientViewRequest(string signerEmail, string signerName, string envelopeId, string documentId)
        {


            // Data for this method
            // signerEmail 
            // signerName
            // dsPingUrl -- class global
            // signerClientId -- class global
            // dsReturnUrl -- class global

            RecipientViewRequest viewRequest = new RecipientViewRequest();
            // Set the url where you want the recipient to go once they are done signing
            // should typically be a callback route somewhere in your app.
            // The query parameter is included as an example of how
            // to save/recover state information during the redirect to
            // the DocuSign signing ceremony. It's usually better to use
            // the session mechanism of your web framework. Query parameters
            // can be changed/spoofed very easily.




            viewRequest.ReturnUrl = "https://localhost:44339/Home/Privacy";

            // How has your app authenticated the user? In addition to your app's
            // authentication, you can include authenticate steps from DocuSign.
            // Eg, SMS authentication
            viewRequest.AuthenticationMethod = "none";

            // Recipient information must match embedded recipient info
            // we used to create the envelope.
            viewRequest.Email = signerEmail;
            viewRequest.UserName = signerName;
            viewRequest.ClientUserId = "A111";

            // DocuSign recommends that you redirect to DocuSign for the
            // Signing Ceremony. There are multiple ways to save state.
            // To maintain your application's session, use the pingUrl
            // parameter. It causes the DocuSign Signing Ceremony web page
            // (not the DocuSign server) to send pings via AJAX to your
            // app,
            viewRequest.PingFrequency = "600"; // seconds
                                               // NOTE: The pings will only be sent if the pingUrl is an https address
            viewRequest.PingUrl = "https://localhost:44339/"; // optional setting

            return viewRequest;
        }

        [HttpGet]
        public void MyJwt()
        {
            Response.Redirect("https://account-d.docusign.com/oauth/auth?response_type=code&" +
                               "scope=signature%20impersonation&client_id=945396b2-0e7e-45fd-aa19-7335574b83da&" +
                               "state=a39fh23hnf23&" +
                               "redirect_uri=https://localhost:44339/Account/AfterMyJwt");
        }

        [HttpGet]
        public void AfterMyJwt(string code, string state)
        {
            // var a = await _apiClient.CallApiAsync("/v2.1/accounts/11486799/users", RestSharp.Method.GET, null, null, null, null, null, null, null);
            this._authToken = _apiClient.GenerateAccessToken("945396b2-0e7e-45fd-aa19-7335574b83da", "cf5f486e-f510-4502-8a9c-1248c86e3ce2", code);


            //// Call DocuSign to create the envelope                   
            var config = new Configuration("https://demo.docusign.net/restapi");
            config.AddDefaultHeader("Authorization", "Bearer " + this._authToken.access_token);
            AuthenticationApi authApi = new AuthenticationApi(_apiClient);
            LoginInformation loginInfo = authApi.Login();
            //// user might be a member of multiple accounts
            //var accountId = loginInfo.LoginAccounts[0];
            var token = _apiClient.RequestJWTUserToken("945396b2-0e7e-45fd-aa19-7335574b83da",
                  "8c5906bc-78c1-4edb-abf7-dfab5cd474d5",
                  eSign.Client.Auth.OAuth.Demo_OAuth_BasePath,
                   DSHelper.ReadFileContent(DSHelper.PrepareFullPrivateKeyFilePath("private.key"))
                   , 1
                  );
        }

    }
}
